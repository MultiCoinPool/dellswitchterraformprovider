package main

import (
	"bitbucket.org/MultiCoinPool/dellswitchconfig"
	"fmt"
	"github.com/hashicorp/terraform/helper/schema"
	"strconv"
)

func resourceServer() *schema.Resource {
	return &schema.Resource{
		Create: resourceVlanCreate,
		Read:   resourceVlanRead,
		Delete: resourceVlanDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},
		Schema: map[string]*schema.Schema{
			"vlan": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
				ForceNew: true,
			},
		},
	}
}

func resourceVlanCreate(d *schema.ResourceData, m interface{}) error {
	address := d.Get("vlan").(int)
	DellSwitch.AddVlan(address)
	d.SetId(fmt.Sprintf("%d", address))
	return nil
}

func resourceVlanRead(d *schema.ResourceData, m interface{}) error {
	q, _ := strconv.Atoi(d.Id())
	if v, ok := DellSwitch.GetVlans()[q]; ok {
		d.Set("vlan", v.Id)
		d.SetId(fmt.Sprintf("%d", v.Id))
	} else {
		d.SetId("")
	}
	return nil
}

func resourceVlanDelete(d *schema.ResourceData, m interface{}) error {
	if _, ok := DellSwitch.GetVlans()[d.Get("vlan").(int)]; ok {
		DellSwitch.DeleteVlan(d.Get("vlan").(int))
	}
	return nil
}
