package main

import (
	"bitbucket.org/MultiCoinPool/dellswitchconfig"
	"github.com/hashicorp/terraform/helper/schema"
)

func resourcePort() *schema.Resource {
	return &schema.Resource{
		Create: resourcePortCreate,
		Update: resourcePortUpdate,
		Read:   resourcePortRead,
		Delete: resourcePortDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},
		Schema: map[string]*schema.Schema{
			"port_id": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"vlan": {
				Type:     schema.TypeSet,
				Optional: true,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"vlan": {
							Type:     schema.TypeInt,
							Required: true,
						},
						"tagged": {
							Type:     schema.TypeBool,
							Required: true,
						},
						"pvid": {
							Type:     schema.TypeBool,
							Default:  false,
							Optional: true,
						},
					},
				},
			},
		},
	}
}

func resourcePortCreate(d *schema.ResourceData, m interface{}) error {
	return nil
}

func resourcePortUpdate(d *schema.ResourceData, m interface{}) error {
	if d.HasChange("vlan") {
		pid := d.Get("port_id").(string)
		oldc, newc := d.GetChange("vlan")

		for _, v := range oldc.(*schema.Set).List() {
			nv := v.(map[string]interface{})
			DellSwitch.DeleteVlanPort(pid, nv["vlan"].(int))
		}

		for _, v := range newc.(*schema.Set).List() {
			nv := v.(map[string]interface{})
			DellSwitch.AddVlanPort(pid, nv["vlan"].(int), nv["tagged"].(bool), nv["pvid"].(bool))
			//log.Printf("Port: %s | Vlan: %d | Tag: %t | pvid: %t\n", d.Get("port_id"), nv["vlan"].(int), nv["tagged"].(bool), nv["pvid"].(bool))
		}
		//return errors.New(fmt.Sprintf("Old: %+v | New: %+v", old, new3))
	}
	return nil
}

func resourcePortRead(d *schema.ResourceData, m interface{}) error {
	if v, ok := DellSwitch.GetPorts()[d.Id()]; ok {
		d.Set("port_id", v.Port)
		vlan := make([]interface{}, 0)
		pvid := v.PortVlan.Pvid
		for _, v2 := range v.PortVlan.Vlans {
			temp := make(map[string]interface{})
			temp["vlan"] = v2.Vlan
			temp["tagged"] = v2.Egress != "Untagged"
			temp["pvid"] = v2.Vlan == pvid
			vlan = append(vlan, temp)
		}
		d.Set("vlan", vlan)
	} else {
		d.SetId("")
	}
	return nil

}

func resourcePortDelete(d *schema.ResourceData, m interface{}) error {
	/*if _, ok := DellSwitch.GetVlans()[d.Get("vlan").(int)]; ok {
		DellSwitch.DeleteVlan(d.Get("vlan").(int))
	}*/
	return nil
}
