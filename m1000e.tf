provider "dell" {
  alias = "d2"
  username = "root"
  ip       = "192.168.1.61"
  password = "calvin"
}

resource "dell_switch_port" "mp7" {
  provider = dell.d2
  port_id = "Gi1/0/7"
  vlan {
    vlan   = dell_switch_vlan.mv1.vlan
    tagged = false
    pvid   = true
  }
  vlan {
    vlan   = dell_switch_vlan.mv200.vlan
    tagged = true
    pvid   = false
  }
}

resource "dell_switch_vlan" "mv200" {
  provider = dell.d2
  vlan = 200
}

resource "dell_switch_vlan" "mv1" {
  provider = dell.d2
  vlan = 1
}