provider "dell" {
  username = "admin"
  ip       = "192.168.1.7"
  password = "calvin"
}

resource "dell_switch_vlan" "v300" {
  vlan = 300
}

resource "dell_switch_vlan" "v1" {
  vlan = 1
}

resource "dell_switch_vlan" "v200" {
  vlan = 200
}

resource "dell_switch_port" "p9" {
  port_id = "1/g9"
  vlan {
    vlan   = dell_switch_vlan.v200.vlan
    tagged = true
    pvid   = false
  }
  vlan {
    vlan   = dell_switch_vlan.v1.vlan
    tagged = false
    pvid   = true
  }
  vlan {
    vlan   = dell_switch_vlan.v300.vlan
    tagged = true
  }
}

resource "dell_switch_port" "p8" {
  port_id = "1/g8"

  vlan {
    vlan   = dell_switch_vlan.v300.vlan
    tagged = false
    pvid = true
  }
}