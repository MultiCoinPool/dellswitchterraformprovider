package main

import (
	"bitbucket.org/MultiCoinPool/dellswitchconfig"
	"github.com/hashicorp/terraform/helper/schema"
)

func Provider() *schema.Provider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"username": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Login username",
			},
			"password": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Login password",
				Sensitive:   true,
			},
			"ip": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Dell Switch management IP",
			},
		},
		ConfigureFunc: ConfigureProvider,
		ResourcesMap: map[string]*schema.Resource{
			"dell_switch_vlan": resourceServer(),
			"dell_switch_port": resourcePort(),
		},
	}
}

func ConfigureProvider(d *schema.ResourceData) (interface{}, error) {
	DellSwitch.ClientInit(d.Get("ip").(string), d.Get("username").(string), d.Get("password").(string))
	return nil, nil
}
